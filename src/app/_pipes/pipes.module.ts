import { NgModule } from '@angular/core';
import { IonicModule } from '../../../node_modules/@ionic/angular';
import { Autosize } from './autosize';

@NgModule({
  declarations: [
    Autosize
  ],
  imports: [
    IonicModule
  ],
  exports: [
    Autosize
  ]
})
export class PipeModule {}
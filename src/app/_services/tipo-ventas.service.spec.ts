import { TestBed } from '@angular/core/testing';

import { TipoVentasService } from './tipo-ventas.service';

describe('TipoVentasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoVentasService = TestBed.get(TipoVentasService);
    expect(service).toBeTruthy();
  });
});

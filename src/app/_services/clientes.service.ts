import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { path } from '../config.module';
import "rxjs/add/operator/toPromise";

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http: Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //GET ALL
  public getAll():Promise<any> {
    let url = `${this.basePath}/api/clientes`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //CREATE
  public create(form):Promise<any> {
    let url = `${this.basePath}/api/clientes`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //CREATE
  public assignament(form):Promise<any> {
    let url = `${this.basePath}/api/clienteusuario`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //DELETE
  public delete(id):Promise<any> {
    let url = `${this.basePath}/api/clientes/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //UPDATED
  public update(form):Promise<any> {
    let url = `${this.basePath}/api/clientes/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET SINGLE
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}/api/clientes/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET SINGLE
  public getAllByUser(id:number):Promise<any> {
    let url = `${this.basePath}/api/usuarios/${id}/clientes`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET NEAR
  public getNear(id:number, lat:any, lng:any,):Promise<any> {
    let url = `${this.basePath}/api/near/${id}/clientes?latitude=${lat}&longitude=${lng}&distance=500`;
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}

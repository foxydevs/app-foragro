import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '../../../node_modules/@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  //PROPIEDADES 
  isLoading = false;

  constructor(public alertController: AlertController,
  public loadingController: LoadingController,
  public toastController: ToastController
  ) { }

  async confirmation(title:any, description:any) {
    const alert = await this.alertController.create({
      header: title,
      message: description,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentToast(msg:any) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1000
    });
    toast.present();
  }

  async present(msg:string) {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
      message: msg
    }).then((a) => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => {
            
          }).catch(error => {
            console.log(error)
          })
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => {
      
    });
  }
}

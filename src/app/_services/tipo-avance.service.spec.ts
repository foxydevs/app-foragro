import { TestBed } from '@angular/core/testing';

import { TipoAvanceService } from './tipo-avance.service';

describe('TipoAvanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoAvanceService = TestBed.get(TipoAvanceService);
    expect(service).toBeTruthy();
  });
});

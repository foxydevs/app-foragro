import { TestBed } from '@angular/core/testing';

import { ClienteUsuariosService } from './cliente-usuarios.service';

describe('ClienteUsuariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClienteUsuariosService = TestBed.get(ClienteUsuariosService);
    expect(service).toBeTruthy();
  });
});

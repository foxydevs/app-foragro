import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { path } from '../config.module';
import "rxjs/add/operator/toPromise";

@Injectable({
  providedIn: 'root'
})
export class FormVisitaService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http: Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //GET ALL
  public getAll():Promise<any> {
    let url = `${this.basePath}/api/formvisita`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //CREATE
  public create(form):Promise<any> {
    let url = `${this.basePath}/api/formvisita`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //DELETE
  public delete(id):Promise<any> {
    let url = `${this.basePath}/api/formvisita/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //UPDATED
  public update(form):Promise<any> {
    let url = `${this.basePath}/api/formvisita/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET SINGLE
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}/api/formvisita/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET SINGLE
  public getAllClient(id:number):Promise<any> {
    let url = `${this.basePath}/api/clientes/${id}/formvisita`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
  //http://backend.foxylabs.xyz/foragro/public/api/clientes/2/formvisita/1?fecha=2018-11-13

  //GET SINGLE
  public getSingleState(id:number, state:number, fecha:any):Promise<any> {
    let url = `${this.basePath}/api/clientes/${id}/formvisita/${state}?fecha=${fecha}`
    console.log(url)
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}

import { TestBed } from '@angular/core/testing';

import { CamionesService } from './camiones.service';

describe('CamionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CamionesService = TestBed.get(CamionesService);
    expect(service).toBeTruthy();
  });
});

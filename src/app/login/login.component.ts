import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../_services/auth.service';
import { MessagesService } from '../_services/messages.service';
import { RecoveryComponent } from '../recovery/recovery.component';

declare var $: any

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  auth:any
  closeResult: string;
  pictureLogin:string = 'http://www.freejpg.com.ar/asset/900/ad/ad6b/F100007797.jpg';
  pictureApp:string = 'http://www.foragro.com/library/images/logo.svg';
  user:any = {
    username:'',
    password: ''
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private _service: NotificationsService,
    private mainService: AuthService,
    private messagesService: MessagesService,
    private modalController: ModalController
  ) { }

  public options = {
    position: ["bottom", "right"],
    timeOut: 3000,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    lastOnBottom: false,
    preventDuplicates: true,
    animate: "scale",
    maxLength: 400
  };

  create(text) {
    this._service.error('Error!','Ha ocurrido un error.'+text)
  }
  goToRoute(route:string) {
    this.navCtrl.navigateForward(route);
  }

  logIn(){
    this.mainService.authentication(this.user)
    .then(response => {
      localStorage.setItem('currentUser', response.username);
      localStorage.setItem('currentId', response.id);
      if(response.empleados) {
        localStorage.setItem('currentFirstName', response.empleados.nombre);
        localStorage.setItem('currentLastName', response.empleados.apellido);
      }
      localStorage.setItem('currentPicture', response.picture);
      localStorage.setItem('currentEmail', response.email);
      localStorage.setItem('currentState', response.estado);
      localStorage.setItem('currentEmpleadoID', response.empleado);
      localStorage.setItem('currentCamiones', JSON.stringify(response.camiones));
      this.goToRoute('home');
    }).catch(error => {
      this.messagesService.presentToast('Usuario o contraseña incorrectos.');
    })
    console.clear
    
  }

  ngOnInit() {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: RecoveryComponent
    });
    modal.onDidDismiss().then((data) => {
    });
    return await modal.present();
  }

}

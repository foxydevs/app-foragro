import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../_services/usuarios.service';
import { MessagesService } from '../_services/messages.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss']
})
export class RecoveryComponent implements OnInit {
  //PROPIEDADES
  private pictureLogin:string = 'http://www.freejpg.com.ar/asset/900/ad/ad6b/F100007797.jpg';
  private btnDisabled:boolean;
  private data = {
    username: ''
  }
  constructor(private mainService: UsuariosService,
    private messageService: MessagesService,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  //RECOVERY
  public resetPassword() {
    if(this.data.username) {
      this.btnDisabled = true;
      this.mainService.resetpassword(this.data)
      .then(response => {
        this.messageService.confirmation('Contraseña Recuperada', 'Te hemos enviado un correo a: ' + response.email);
        this.closeModal();
      }).catch(error => {
        this.messageService.presentToast('Usuario no encontrado.');
        this.btnDisabled = false;
        console.log(error)
      })
    } else {
      this.messageService.presentToast('El correo es requerido.');
    }
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

}

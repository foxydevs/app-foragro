import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";
import { RouteReuseStrategy } from '@angular/router';
import { LoadersCssModule } from 'angular2-loaders-css';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { RecoveryComponent } from './recovery/recovery.component';

import { AuthGuard } from './_guards/auth.guard';
import { HomeGuard } from './_guards/home.guard';
import { PipeModule } from './_pipes/pipes.module';
import { AuthService } from './_services/auth.service';
import { UbicacionService } from './_services/ubicacion.service';

@NgModule({
  declarations: [
    AppComponent, 
    LoginComponent, 
    RecoveryComponent
  ],
  entryComponents: [

  ],
  imports: [
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    LoadersCssModule,
    SimpleNotificationsModule.forRoot(),
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    PipeModule
  ],
  providers: [
    StatusBar,
    AuthGuard,
    HomeGuard,
    SplashScreen,
    AuthService,
    Geolocation,
    UbicacionService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}

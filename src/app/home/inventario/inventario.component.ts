import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MessagesService } from '../../_services/messages.service';
import { InventarioService } from '../../_services/inventario.service';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit {
  titulo = 'Inventario'
  Table:any;
  constructor(
    private router: Router,
    private location: Location,
    private mainService: InventarioService,
    private messageService: MessagesService,
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  //CARGAR
  public getAll() {
    this.messageService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.Table = response;
      this.messageService.dismiss();
    }).catch(error => {
      console.log(error)
      this.messageService.dismiss();
    })
  }

}

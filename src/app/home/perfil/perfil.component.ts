import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { ModalPerfilComponent } from './modal-perfil/modal-perfil.component';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  titulo = 'Mi Perfil'
  picture = localStorage.getItem('currentPicture')
  nombre = localStorage.getItem('currentFirstName') + ' ' + localStorage.getItem('currentLastName')
  email = localStorage.getItem('currentEmail')

  constructor(public router: Router,
    public location: Location,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  getAll() {
    this.picture = localStorage.getItem('currentPicture')
    this.nombre = localStorage.getItem('currentFirstName') + ' ' + localStorage.getItem('currentLastName')
    this.email = localStorage.getItem('currentEmail')
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      this.getAll();
    });
    return await modal.present();
  }

}

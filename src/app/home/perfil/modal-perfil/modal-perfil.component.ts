import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { UsuariosService } from '../../../_services/usuarios.service';
import { EmpleadosService } from '../../../_services/empleados.service';
import { MessagesService } from '../../../_services/messages.service';
import { path } from '../../../config.module';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-perfil',
  templateUrl: './modal-perfil.component.html',
  styleUrls: ['./modal-perfil.component.scss']
})
export class ModalPerfilComponent implements OnInit {
  parameter:any;
  titulo:any;
  table:any = [];
  btnDisabled:boolean;
  btnDisabled2:boolean;
  basePath:string = path.path;
  changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: +localStorage.getItem('currentId')
  }
  data = {
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: '',
    celular: '',
    id: +localStorage.getItem('currentEmpleadoID')
  }
  data2 = {
    pais: '',
    picture: '',
    id: +localStorage.getItem('currentId')
  }

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: UsuariosService,
    private secondService: EmpleadosService,
    private messagesService: MessagesService,
  ) { 
  }

  ngOnInit() {
    this.table = JSON.parse(localStorage.getItem('currentPaises'))
    this.parameter = this.navParams.get('value');
    if(this.parameter == '1') {
      this.titulo = 'Actualizar Perfil';
      this.getSingle(this.data2.id)
      this.getSingleEmpleado(this.data.id)
    } else if(this.parameter == '2') {
      this.titulo = 'Cambiar Contraseña';
    }
  }

  saveChanges(e:any) {
    console.log('INSERT', e)
    this.data2.pais = e.pais;
    this.updated(this.data2);
    this.updatedEmpleado(this.data);
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.btnDisabled = true;
    this.mainService.update(formValue)
    .then(response => {
      this.messagesService.confirmation('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
      this.closeModal();
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  updatedEmpleado(formValue:any) {
    this.btnDisabled = true;
    this.secondService.update(formValue)
    .then(response => {
      localStorage.setItem('currentFirstName', response.nombre);
      localStorage.setItem('currentLastName', response.apellido);
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //CARGAR USUARIO
  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .then(response => {
      this.data2.pais = response.pais;
      this.data2.picture = response.picture;
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR USUARIO
  public getSingleEmpleado(id:any) {
    this.messagesService.present('Cargando...');
    this.secondService.getSingle(id)
    .then(response => {
      this.data = response;
      this.messagesService.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.messagesService.presentToast('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.btnDisabled2 = true;
          this.mainService.changePassword(this.changePassword)
          .then(response => {
            this.closeModal();
            this.messagesService.confirmation('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
          }).catch(error => {
            this.btnDisabled2 = false;
            this.messagesService.presentToast('Contraseña inválida.')
          })
        } else {
          this.messagesService.presentToast('Las contraseñas no coinciden.');
        }
      } else {
        this.messagesService.presentToast('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}/api/usuarios/${this.data2.id}/upload/avatar`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",respuesta.picture)
            localStorage.setItem('currentPicture', respuesta.picture);
            $("#"+id).val('')
        });
      } else {
        this.messagesService.presentToast('La imagen es demasiado grande.')
      }
    } else {
      this.messagesService.presentToast('El tipo de imagen no es válido.');
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { HomePage } from './home.page';
import { TerminadasComponent } from './terminadas/terminadas.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

//SERVICIOS
import { CamionesService } from './../_services/camiones.service';
import { ClientesService } from './../_services/clientes.service';
import { EmpleadosService } from './../_services/empleados.service';
import { UsuariosService } from './../_services/usuarios.service';
import { MessagesService } from '../_services/messages.service';
import { FormVisitaService } from '../_services/form-visita.service';
import { TipoClienteService } from '../_services/tipo-cliente.service';
import { TipoAvanceService } from '../_services/tipo-avance.service';
import { PaisesService } from '../_services/paises.service';
import { InventarioService } from '../_services/inventario.service';
import { ProductosService } from '../_services/productos.service';
import { TipoVentasService } from '../_services/tipo-ventas.service';
import { ClienteUsuariosService } from '../_services/cliente-usuarios.service';
import { VentasService } from '../_services/ventas.service';
import { SectorService } from '../_services/sector.service';

import { VisitasComponent } from './visitas/visitas.component';
import { PlanificacionComponent } from './planificacion/planificacion.component';
import { ModalPlanificacionComponent } from './planificacion/modal-planificacion/modal-planificacion.component';
import { ModalVisitasComponent } from './visitas/modal-visitas/modal-visitas.component';
import { VisitasDetalleComponent } from './visitas/visitas-detalle/visitas-detalle.component';
import { RutaTerminadaDetalleComponent } from './ruta-terminada-detalle/ruta-terminada-detalle.component';
import { VentasComponent } from './ventas/ventas.component';
import { ModalProductosComponent } from './ventas/modal-productos/modal-productos.component';
import { ModalVentasComponent } from './ventas/modal-ventas/modal-ventas.component';
import { InventarioComponent } from './inventario/inventario.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ModalClientesComponent } from './clientes/modal-clientes/modal-clientes.component';
import { RutaDetallePlanificacionComponent } from './ruta-detalle-planificacion/ruta-detalle-planificacion.component';
import { ModalRutaDetallePlanificacionComponent } from './ruta-detalle-planificacion/modal-ruta-detalle-planificacion/modal-ruta-detalle-planificacion.component';
import { OfflineComponent } from './offline/offline.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ModalPerfilComponent } from './perfil/modal-perfil/modal-perfil.component';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CamionesComponent } from './camiones/camiones.component';
import { PlanificacionCamionesComponent } from './planificacion-camiones/planificacion-camiones.component';
import { VisitasCamionesComponent } from './visitas-camiones/visitas-camiones.component';
import { CamionComponent } from './camion/camion.component';

//COMPONENTES NATIVOS
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Network } from '@ionic-native/network/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'terminadas',
        component: TerminadasComponent
      },
      {
        path: 'detalle-planificacion/:id',
        component: RutaDetallePlanificacionComponent
      },
      {
        path: 'visita-detalle/:id',
        component: VisitasDetalleComponent
      },
      {
        path: 'detalle-terminadas/:id',
        component: RutaTerminadaDetalleComponent
      },
      {
        path: 'visitas',
        component: VisitasComponent
      },
      {
        path: 'planificacion',
        component: PlanificacionComponent
      },
      {
        path: 'ventas/:id',
        component: VentasComponent
      },
      {
        path: 'inventario',
        component: InventarioComponent
      },
      {
        path: 'clientes',
        component: ClientesComponent
      },
      {
        path: 'offline',
        component: OfflineComponent
      },
      {
        path: 'perfil',
        component: PerfilComponent
      },
      {
        path: 'camiones',
        component: CamionesComponent
      },
      {
        path: 'camion/:id',
        component: CamionComponent
      },
      {
        path: 'visitas-camiones',
        component: VisitasCamionesComponent
      },
      {
        path: 'planificacion-camiones',
        component: PlanificacionCamionesComponent
      },
      {
        path: 'ruta-terminada-detalle',
        component: RutaTerminadaDetalleComponent
      },
    ])
  ],
  declarations: [
    HomePage, 
    TerminadasComponent,
    VisitasComponent, 
    PlanificacionComponent,
    ModalPlanificacionComponent,
    ModalVisitasComponent,
    VisitasDetalleComponent,
    RutaTerminadaDetalleComponent,
    ModalVentasComponent,
    VentasComponent,
    ModalProductosComponent,
    InventarioComponent,
    ClientesComponent,
    ModalClientesComponent,
    RutaDetallePlanificacionComponent,
    ModalRutaDetallePlanificacionComponent,
    OfflineComponent,
    PerfilComponent,
    ModalPerfilComponent,
    CamionesComponent,
    PlanificacionCamionesComponent,
    VisitasCamionesComponent,
    CamionComponent,
  ],
  entryComponents: [
    HomePage, 
    TerminadasComponent,
    VisitasComponent,
    PlanificacionComponent,
    ModalPlanificacionComponent,
    ModalVisitasComponent,
    VisitasDetalleComponent,
    RutaTerminadaDetalleComponent,
    ModalVentasComponent,
    VentasComponent,
    ModalProductosComponent,
    InventarioComponent,
    ClientesComponent,
    ModalClientesComponent,
    RutaDetallePlanificacionComponent,
    ModalRutaDetallePlanificacionComponent,
    OfflineComponent,
    PerfilComponent,
    ModalPerfilComponent,
    CamionesComponent,
    PlanificacionCamionesComponent,
    VisitasCamionesComponent,
    CamionComponent,
  ],
  providers:[
    CamionesService,
    ClientesService,
    EmpleadosService,
    UsuariosService,
    MessagesService,
    FormVisitaService,
    TipoClienteService,
    TipoVentasService,
    TipoAvanceService,
    ClienteUsuariosService,
    VentasService,
    ProductosService,
    InventarioService,
    PaisesService,
    SectorService,
    BackgroundGeolocation,
    //QRScanner,
    BarcodeScanner,
    //NETWORK
    Network,
    //NATIVE GEOLOCATION
    Geolocation,
    NativeGeocoder,
    LaunchNavigator
  ]
})
export class HomePageModule {}

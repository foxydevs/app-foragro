import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse} from '@ionic-native/background-geolocation/ngx';
import { MessagesService } from '../_services/messages.service';
import { UbicacionService } from '../_services/ubicacion.service';
import { TipoClienteService } from '../_services/tipo-cliente.service';
import { TipoAvanceService } from '../_services/tipo-avance.service';
import { ObjetivosService } from '../_services/objetivos.service';
import { CultivosService } from '../_services/cultivos.service';
import { TipoVisitaService } from '../_services/tipo-visita.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { PaisesService } from '../_services/paises.service';
import { SectorService } from '../_services/sector.service';
import { ModalPerfilComponent } from './perfil/modal-perfil/modal-perfil.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  titulo:string = "Rutas"
  Table: any;
  selectedData: any;
  selectedTag: any = "ventas";
  pictureLogin:string = 'http://www.freejpg.com.ar/asset/900/ad/ad6b/F100007797.jpg';
  pictureLogin2:string = 'https://i.pinimg.com/originals/39/6b/f8/396bf8d3b496f5e9ab5347c2a5b14bbf.jpg';
  logs: string[] = [];
  camiones: any[] = [];
  btnCamion:boolean = true;
  //PROPIEDADES GOOGLE MAPS
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];
  marker:any;
  labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  labelIndex = 0;
  codeCountry:any;
  slideOpts = {
    effect: 'fade',
    spaceBetween: 0,
  };
  constructor(
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    private mainService: UbicacionService,
    private backgroundGeolocation: BackgroundGeolocation,
    public messagesService: MessagesService,
    public secondService: TipoClienteService,
    public thirdService: TipoAvanceService,
    public fourthService: ObjetivosService,
    public fifthService: CultivosService,
    public sixthService: TipoVisitaService,
    public seventhService: PaisesService,
    public eightService: SectorService,
    public alertController: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public modalController: ModalController
  ) {}

  //CAMIONES
  getTrucks() {
    this.camiones = JSON.parse(localStorage.getItem('currentCamiones'));
    if(this.camiones.length == 0) {
      this.btnCamion = true;
    } else if(this.camiones.length > 0) {
      this.btnCamion = false;
    }
  }

  //OPEN FORM
  openTruck() {
    if(this.camiones.length == 1) {
      this.goToRoute('home/camion/' + this.camiones[0].id);
    } else if(this.camiones.length > 1) {
      this.goToRoute('home/camiones');
    }
  }

  //GET POSITION
  getPosition() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        let data = {
          nombre: result[0].countryName,
          codigo: result[0].countryCode,
          countryCode: result[0].countryCode,
          countryName: result[0].countryName,
          postalCode: result[0].postalCode,
          administrativeArea: result[0].administrativeArea,
          subAdministrativeArea: result[0].subAdministrativeArea,
          locality: result[0].locality,
        }
        this.getFilter(result[0].countryCode, data);
        this.codeCountry = result[0].countryCode;
        /*console.log(JSON.stringify(result[0]))
        alert(JSON.stringify(result[0]))
        alert(result[0])
        alert("Codigo" + result[0].countryCode)
        this.codeCountry = result[0].countryCode
        alert("Nombre" + result[0].countryName)
        alert("Postal" + result[0].postalCode)
        alert("Localidad" + result[0].locality)*/
      }).catch((error: any) => {
        console.log(error)
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  public getFilter(data:any, form:any) {
    this.seventhService.getFilter(data)
    .then(response => {
      localStorage.setItem('currentPais', response.id);
      if(response.length == 0) {
        this.createCountry(form);
      }
    }).catch(error => {
      localStorage.setItem('currentPais', '');
      console.clear;
    })
  }

  //AGREGAR
  createCountry(formValue:any) {
    this.seventhService.create(formValue)
    .then(response => {
      localStorage.setItem('currentPais', response.id);
    }).catch(error => {
      localStorage.setItem('currentPais', '');
      console.log(error)
    });
  }

  ngOnInit() {
    if(localStorage.getItem('currentState') == '21') {
      this.presentModal(2);
    }
    this.startBackgroundGeolocation();
    this.getPosition();
    this.getTrucks();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      this.getAll();
    });
    return await modal.present();
  }

  ionViewDidLoad() {
    
  }

  getAll() {
    this.getAllSecond();
    this.getAllThird();
    this.getAllFourth();
    this.getAllFifth();
    this.getAllSixth();
    this.getAllEigth();
  }

  //CERRAR SESION
  async logOut() {
    if(JSON.parse(localStorage.getItem('currentClientOffline')) ||
    JSON.parse(localStorage.getItem('currentVisitaOffline')) || 
    JSON.parse(localStorage.getItem('currentVisitaDiarioOffline'))) {
      if(JSON.parse(localStorage.getItem('currentClientOffline')).length > 0 ||
      JSON.parse(localStorage.getItem('currentVisitaOffline')).length  > 0 || 
      JSON.parse(localStorage.getItem('currentVisitaDiarioOffline')).length  > 0) {
        const alert = await this.alertController.create({
          header: 'Cerrar Sesión',
          message: '¿Desea cerrar sesión?, Hay información almacenada sin conexión, si cierra sesión no podra recuperar la información almacenada.',
          buttons: [{
            text: 'Cancelar',
            handler: () => {
    
            }
          },{
            text: 'Aceptar',
            handler: () => {
              localStorage.clear();
              this.goToRoute('login')
            }
          }]
        });
        alert.present();
      }
    } else {
      localStorage.clear();
      this.goToRoute('login')
    }    
  }

  startBackgroundGeolocation(){
    this.backgroundGeolocation.isLocationEnabled()
    .then((rta) =>{
      if(rta){
        this.start();
      }else {
        this.backgroundGeolocation.showLocationSettings();
      }
    }).catch(error =>{
      console.log(error)
    })
  }
  
  start(){
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 100,
      distanceFilter: 1000,
      stopOnTerminate: false,
      // Android only section
      locationProvider: 1,
      startForeground: true,
      interval: 1800000,
      activitiesInterval: 1800000,
      fastestInterval: 1800000,
      stopOnStillActivity: false
    };
  
    console.log('start');
    let id = +localStorage.getItem('currentId');
    this.backgroundGeolocation
    .configure(config)
    .subscribe((location: BackgroundGeolocationResponse) => {
      console.log(location);
      this.logs.push(`${location.latitude},${location.longitude}`);
      let data = {
        latitude: location.latitude,
        longitude: location.longitude,
        direccion: '1 KM, 100 radius',
        usuario: id,
        estado: 1,
        tipo: 1
      }
      this.create(data);
      console.log(this.logs)
    },
    error => {
      console.log(error)
    });
  
    // start recording location
    this.backgroundGeolocation.start();
  
  }

  stopBackgroundGeolocation(){
    this.backgroundGeolocation.stop();
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
    }).catch(error => {
      if(error) {
        this.messagesService.presentToast('Ha ocurrido un error.');
      }
      console.clear
    });
  }

  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      localStorage.setItem('currentTipoCliente', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }
  
  public getAllThird(){
    this.thirdService.getAll()
    .then(response => {
      localStorage.setItem('currentTipoAvance', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

  //GET ALL SERVICES
  public getAllFourth(){
    this.fourthService.getAll()
    .then(response => {
      localStorage.setItem('currentTipoObjetivos', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

  public getAllFifth(){
    this.fifthService.getAll()
    .then(response => {
      localStorage.setItem('currentTipoCultivo', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

  public getAllSixth(){
    this.sixthService.getAll()
    .then(response => {
      localStorage.setItem('currentTipoVisita', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

  public getAllSeventh(){
    this.seventhService.getAll()
    .then(response => {
      localStorage.setItem('currentPaises', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

  public getAllEigth(){
    this.eightService.getAll()
    .then(response => {
      localStorage.setItem('currentSectores', JSON.stringify(response));
    }).catch(error => {
      console.clear;
    })
  }

}

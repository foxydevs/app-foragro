import { Component, OnInit } from '@angular/core';
import { FormVisitaService } from '../../../_services/form-visita.service';
import { TipoClienteService } from '../../../_services/tipo-cliente.service';
import { TipoAvanceService } from '../../../_services/tipo-avance.service';
import { MessagesService } from '../../../_services/messages.service';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Location } from '@angular/common';
import { ClientesService } from '../../../_services/clientes.service';
import { ModalVisitasComponent } from '../modal-visitas/modal-visitas.component';
import { ModalController } from '../../../../../node_modules/@ionic/angular';
import { ObjetivosService } from '../../../_services/objetivos.service';
import { CultivosService } from '../../../_services/cultivos.service';

@Component({
  selector: 'app-visitas-detalle',
  templateUrl: './visitas-detalle.component.html',
  styleUrls: ['./visitas-detalle.component.scss']
})
export class VisitasDetalleComponent implements OnInit {
  //PROPIEDADES
  disabledBtn:boolean = false;
  titulo:string = "Detalle"
  Table:any;
  selectedData:any;
  parameter:any;
  selectedTag:any = "plan";
  comentario:any;
  tipoCliente:any = [];
  tipoAvance:any = [];
  cultivos:any = [];
  objetivos:any = [];
  visitas:any = [];
  data = {
    id: '',
    semana: '',
    fecha: new Date().toISOString(),
    hora: '',
    nombre_cliente: '',
    lugar: '',
    departamento: '',
    objetivos: '', 
    cultivo: '',
    observaciones: '',
    tipo: '',
    estado: 1,
    cliente: '',
    tipo_cliente: '',
    tipo_avance: '',
    latitud: '',
    longitud: '',
    logrado: null,
    razon: '',
    usuario: localStorage.getItem('currentId'),
  }

  constructor(public mainService: FormVisitaService,
  public secondService: TipoClienteService,
  public thirdService: TipoAvanceService,
  public fourthService: ClientesService,
  public fifthService: ObjetivosService,
  public sixthService: CultivosService,
  public messagesService: MessagesService,
  public modalController: ModalController,
  public location: Location,
  public router: ActivatedRoute) {
  }

  //ROUTES
  goToBack() {
    this.location.back();
  }
  active(select:any){
    this.selectedTag = select;
  }

  ngOnInit() {
    this.parameter = this.router.snapshot.paramMap.get('id');
    this.data.cliente = this.parameter;
    this.getSingleCliente(this.parameter);
    //this.getAllSecond();
    //this.getAllThird();
    //this.getAllFifth();
    //this.getAllSixth();
    this.tipoCliente = JSON.parse(localStorage.getItem('currentTipoCliente'));
    this.tipoAvance = JSON.parse(localStorage.getItem('currentTipoAvance'));
    this.objetivos = JSON.parse(localStorage.getItem('currentTipoObjetivos'));
    this.cultivos = JSON.parse(localStorage.getItem('currentTipoCultivo'));
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    let fechainicio = (this.data.fecha.replace('T',' ').replace('Z', '')).split(" ")
    this.data.fecha = fechainicio[0];
    this.create(this.data)
  }

  
  //GET SINGLE
  public getSingleCliente(id:any){
    this.fourthService.getSingle(id)
    .then(response => {
      this.data.nombre_cliente = response.nombre + ' ' + response.apellido;
      this.data.latitud = response.latitud;
      this.data.longitud = response.longitud;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR
  public getAll(){
    this.messagesService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.visitas = [];
      this.visitas = response;
      this.visitas.reverse();
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.tipoCliente = response;
    }).catch(error => {
      console.clear;
    })
  }
  
  public getAllThird(){
    this.thirdService.getAll()
    .then(response => {
      this.tipoAvance = response;
    }).catch(error => {
      console.clear;
    })
  }

  public getAllFifth(){
    this.fifthService.getAll()
    .then(response => {
      this.objetivos = response;
    }).catch(error => {
      console.clear;
    })
  }

  public getAllSixth(){
    this.sixthService.getAll()
    .then(response => {
      this.cultivos = response;
    }).catch(error => {
      console.clear;
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .then(response => {
      this.messagesService.confirmation('Planificación Agregada', 'La planificación fue agregada exitosamente.');
    }).catch(error => {
      this.disabledBtn = false;
      this.createOffline(this.data);
      this.messagesService.confirmation('Sin Conexión', 'El formulario fue agregado en la sección SIN CONEXIÓN.');
      console.clear
    });
  }

  //AGREGAR
  createOffline(data:any) {
    let client:any[] = []
    if(localStorage.getItem('currentVisitaOffline')) {
      client = JSON.parse(localStorage.getItem('currentVisitaOffline'))
    }
    client.push(data)
    localStorage.removeItem('currentVisitaOffline')
    localStorage.setItem('currentVisitaOffline', JSON.stringify(client));
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalVisitasComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }
}

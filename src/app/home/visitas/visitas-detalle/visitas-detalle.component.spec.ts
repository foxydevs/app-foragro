import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitasDetalleComponent } from './visitas-detalle.component';

describe('VisitasDetalleComponent', () => {
  let component: VisitasDetalleComponent;
  let fixture: ComponentFixture<VisitasDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitasDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitasDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

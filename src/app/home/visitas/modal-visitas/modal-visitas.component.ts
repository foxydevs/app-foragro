import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../../../_services/messages.service';
import { NavParams, ModalController } from '../../../../../node_modules/@ionic/angular';
import { FormVisitaService } from '../../../_services/form-visita.service';

@Component({
  selector: 'app-modal-visitas',
  templateUrl: './modal-visitas.component.html',
  styleUrls: ['./modal-visitas.component.scss']
})
export class ModalVisitasComponent implements OnInit {
  //PROPIEDADES
  data:any = [];
  parameter:any;

  constructor(
  public mainService:FormVisitaService,
  public messagesService:MessagesService,
  public navParams: NavParams,
  public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingleCliente(this.parameter)
  }

  //GET SINGLE
  public getSingleCliente(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getSingle(id)
    .then(response => {
      this.data = [];
      this.data = response;
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }
}

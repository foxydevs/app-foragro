import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { ModalController, AlertController } from '../../../../node_modules/@ionic/angular';
import { ClientesService } from '../../_services/clientes.service';
import { MessagesService } from '../../_services/messages.service';
import { FormVisitaService } from '../../_services/form-visita.service';

@Component({
  selector: 'app-offline',
  templateUrl: './offline.component.html',
  styleUrls: ['./offline.component.scss']
})
export class OfflineComponent implements OnInit {
  titulo:string = 'Data Offline'
  Table:any = [];
  Table2:any = [];
  Table3:any = [];
  selectedTag:any = 'clientes';
  constructor(private router:Router,
  private location:Location,
  private modalController: ModalController,
  private messagesService: MessagesService,
  private alertController: AlertController,
  private mainService: ClientesService,
  private secondService: FormVisitaService,
  ) { }

  ngOnInit() {
    this.getAll();
  }
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  //GET
  getAll() {
    this.Table = JSON.parse(localStorage.getItem('currentClientOffline'))
    this.Table2 = JSON.parse(localStorage.getItem('currentVisitaOffline'))
    this.Table3 = JSON.parse(localStorage.getItem('currentVisitaDiarioOffline'))
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .then(response => {
    }).catch(error => {
      console.clear
    });
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.Table.splice(this.Table.indexOf(formValue),1)
      localStorage.removeItem('currentClientOffline');
      localStorage.setItem('currentClientOffline', JSON.stringify(this.Table))
      this.messagesService.confirmation('Cliente Agregado', 'El cliente fue agregada exitosamente.');
    }).catch(error => {
      console.log(error)
    });
  }

  //AGREGAR
  create2(formValue:any) {
    this.secondService.create(formValue)
    .then(response => {
      this.Table.splice(this.Table.indexOf(formValue),1)
      localStorage.removeItem('currentVisitaOffline');
      localStorage.setItem('currentVisitaOffline', JSON.stringify(this.Table))
      this.messagesService.confirmation('Cliente Agregado', 'El cliente fue agregada exitosamente.');
    }).catch(error => {
      console.log(error)
    });
  }

  async deleted(e:any) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Desea eliminar la información almacenada previamente?, No se podra recuperar la información almacenada.',
      buttons: [{
        text: 'Cancelar',
        handler: () => {

        }
      },{
        text: 'Aceptar',
        handler: () => {
          this.Table.splice(this.Table.indexOf(e),1)
          localStorage.removeItem('currentClientOffline');
          localStorage.setItem('currentClientOffline', JSON.stringify(this.Table))
        }
      }]
    });
    await alert.present();
  }

  async deleted2(e:any) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Desea eliminar la información almacenada previamente?, No se podra recuperar la información almacenada.',
      buttons: [{
        text: 'Cancelar',
        handler: () => {

        }
      },{
        text: 'Aceptar',
        handler: () => {
          this.Table.splice(this.Table.indexOf(e),1)
          localStorage.removeItem('currentVisitaOffline');
          localStorage.setItem('currentVisitaOffline', JSON.stringify(this.Table))
        }
      }]
    });
    await alert.present();
  }

  async deleted3(e:any) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Desea eliminar la información almacenada previamente?, No se podra recuperar la información almacenada.',
      buttons: [{
        text: 'Cancelar',
        handler: () => {

        }
      },{
        text: 'Aceptar',
        handler: () => {
          this.Table.splice(this.Table.indexOf(e),1)
          localStorage.removeItem('currentVisitaDiarioOffline');
          localStorage.setItem('currentVisitaDiarioOffline', JSON.stringify(this.Table))
        }
      }]
    });
    await alert.present();
  }

  segmentChanged(ev: any) {
    this.selectedTag = ev;
  }

}


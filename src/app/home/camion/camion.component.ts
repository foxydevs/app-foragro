import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController, AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';

@Component({
  selector: 'app-camion',
  templateUrl: './camion.component.html',
  styleUrls: ['./camion.component.scss']
})
export class CamionComponent implements OnInit {
  titulo:string = "Rutas"
  Table: any;
  selectedData: any;
  selectedTag: any = "ventas";
  pictureLogin:string = 'https://i.pinimg.com/originals/39/6b/f8/396bf8d3b496f5e9ab5347c2a5b14bbf.jpg';
  codeCountry:any;

  constructor(
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder
  ) {}

  //GET POSITION
  getPosition() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        this.codeCountry = result[0].countryCode;
      }).catch((error: any) => {
        console.log(error)
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getPosition();
  }

  ionViewDidLoad() {
    
  }
}

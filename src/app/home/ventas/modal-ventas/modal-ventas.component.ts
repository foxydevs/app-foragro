import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, AlertController } from '../../../../../node_modules/@ionic/angular';
import { VentasService } from '../../../_services/ventas.service';
import { MessagesService } from '../../../_services/messages.service';
import { TipoVentasService } from '../../../_services/tipo-ventas.service';
import { ClientesService } from '../../../_services/clientes.service';
import { ModalProductosComponent } from '../modal-productos/modal-productos.component';

@Component({
  selector: 'app-modal-ventas',
  templateUrl: './modal-ventas.component.html',
  styleUrls: ['./modal-ventas.component.scss']
})
export class ModalVentasComponent implements OnInit {
  data = {
    cliente: '',
    usuario: localStorage.getItem('currentId'),
    total: 0,
    fecha: new Date().toISOString(),
    tipo: '',
    tipoPlazo: '',
    plazo: '',
    nombres: '',
    nit: '',
    direccion: '',
    comprobante: 0,
    detalle: [],
    id: ''
  }
  cartProducts:any = [];
  disabledBtn:boolean;
  Ventas:any;
  selectItem:any = 'form';
  
  constructor(
  public modalController: ModalController,
  public navParams: NavParams,
  public mainService: VentasService,
  public secondService: TipoVentasService,
  public thirdService: ClientesService,
  public messagesService: MessagesService,
  public alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.data.id = this.navParams.get('value');
    this.data.cliente = this.navParams.get('cliente');
    this.getAll();
    this.getInvoice();
    this.getSingleCliente(this.data.cliente);
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    let fecha = (this.data.fecha.replace('T',' ').replace('Z', '')).split(" ");
    this.data.fecha = fecha[0];
    if(this.data.detalle) {
      if(this.data.tipo) {
        this.disabledBtn = true;
        this.create(this.data);
      } else {
        this.messagesService.presentToast('El tipo es requerido.');
      }
    } else {
      this.messagesService.presentToast('Los productos son requeridos.');
    }
    
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  active(select:any){
    this.selectItem = select;
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.messagesService.confirmation('Venta Agregada', 'La venta fue agregada exitosamente.');
      this.closeModal();
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CARGAR
  public getAll() {
    this.secondService.getAll()
    .then(response => {
      this.Ventas = response;
    }).catch(error => {
      console.clear
    })
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalProductosComponent
    });
    modal.onDidDismiss().then((data) => {
      if(data.data) {
        this.cartProducts.push(data.data);
        this.sumaTotal();
      }
    });
    return await modal.present();
  }

  //GET SINGLE
  public getSingleCliente(id:any){
    this.thirdService.getSingle(id)
    .then(response => {
      this.data.cliente = response.id;
      this.data.nombres = response.nombre + ' ' + response.apellido;
      this.data.direccion = response.direccion;
      this.data.nit = response.nit;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR FACTURA
  public getInvoice(){
    this.mainService.getComprobante()
    .then(response => {
      let x:number;
      x = +response.comprobante;
      this.data.comprobante = +response.comprobante + 1;
    }).catch(error => {
      console.clear
    })
  }

  public suma(x:number, y:number):number {
    let total = x + y;
    return total;
  }

  async deleteCar(e:any) {
    let confirm = await this.alertCtrl.create({
      header: '¿Desea eliminar el producto del carrito?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cartProducts.splice(this.cartProducts.indexOf(e),1)
            this.sumaTotal();
          }
        }
      ]
    });
    await confirm.present();
  }

  public addMore(e:any) {
    let cant = parseInt(e.cantidad) + 1;
    let sub = cant * e.precioVenta;
    sub.toFixed(2);
    let data = {
      id : e.id,
      codigo : e.codigo,
      nombre : e.nombre,
      descripcion : e.descripcion,
      marca : e.marcaDes,
      tipo : e.tipo,
      producto : e.id,
      precioVenta : e.precioVenta,
      cantidad : cant,
      precioClienteEs : e.precioClienteEs,
      precioDistribuidor : e.precioDistribuidor,
      subtotal : sub
    }
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        this.cartProducts[x] = data;
        this.sumaTotal();
        break; //Stop this loop, we found it!
      }
    }
  }

  public removeProduct(e:any) {
    let cant = parseInt(e.cantidad) - 1;
    let sub = cant * e.precioVenta;
    sub.toFixed(2);
    let data = {
      id : e.id,
      codigo : e.codigo,
      nombre : e.nombre,
      descripcion : e.descripcion,
      marca : e.marcaDes,
      tipo : e.tipo,
      producto : e.id,
      precioVenta : e.precioVenta,
      cantidad : cant,
      precioClienteEs : e.precioClienteEs,
      precioDistribuidor : e.precioDistribuidor,
      subtotal : sub
    }
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        if(this.cartProducts[x].cantidad > 1) {
          this.cartProducts[x] = data;
          this.sumaTotal();
        }        
        break; //Stop this loop, we found it!
      }
    }
  }

  sumaTotal() {
    let a:number = 0;
    let b:number = 0;
    this.data.detalle = this.cartProducts;
    for(let x of this.cartProducts) {
      a = x.subtotal;
      b += a;
    }
    this.data.total = b;
  }

}

import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../../_services/productos.service';
import { MessagesService } from '../../../_services/messages.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-productos',
  templateUrl: './modal-productos.component.html',
  styleUrls: ['./modal-productos.component.scss']
})
export class ModalProductosComponent implements OnInit {
  products:any;
  typesProducts:any = [];
  select:boolean = true;
  form:boolean = false;
  precioClienteEs:boolean = false;
  precioDistribuidor:boolean = false;
  product = {
    id: '',
    codigo: '',
    nombre: '',
    descripcion: '',
    marca: '',
    tipo: '',
    tipos: '',
    producto: '',
    precioVenta: 0,
    cantidad: 0,
    precioClienteEs: '',
    precioDistribuidor: '',
    subtotal: 0
  }

  constructor(private mainService: ProductosService,
    private messageService: MessagesService,
    private modalController: ModalController) { }

  ngOnInit() {
    this.getAllTypes();
    this.getAll();
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }
  //CARGAR
  public getAll() {
    this.messageService.present('Cargando...');
    this.mainService.getAllExistencia()
    .then(response => {
      this.messageService.dismiss();
      this.products = response;
    }).catch(error => {
      this.messageService.dismiss();
      console.clear
    })
  }

  public selectProduct(data:any) {
    this.select = false;
    this.form = true;
    this.product.id = data.id
    this.product.codigo = data.productos.codigo
    this.product.nombre = data.productos.nombre
    this.product.descripcion = data.productos.descripcion
    this.product.marca = data.productos.marcaDes
    this.product.tipo = data.productos.tipo
    this.product.tipos = data.productos.tipos.descripcion
    this.product.producto = data.productos.id
    this.product.precioVenta = data.precioVenta
    this.product.cantidad = 0
    this.product.precioClienteEs = data.precioClienteEs
    this.product.precioDistribuidor = data.precioDistribuidor
    this.precioClienteEs = true;
    this.precioDistribuidor = true;
  }

  public saveChanges() {
    this.product.subtotal = this.product.cantidad * this.product.precioVenta;
    this.modalController.dismiss(this.product);
  }

  //CARGAR
  public getAllTypes() {
    this.mainService.getAllTypes()
    .then(response => {
      this.typesProducts = [];
      this.typesProducts = response;
    })
    .catch(error => {
      console.log(error)
    })
  }
}

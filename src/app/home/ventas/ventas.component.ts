import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { ModalController } from '../../../../node_modules/@ionic/angular';
import { ModalVentasComponent } from './modal-ventas/modal-ventas.component';
import { VentasService } from '../../_services/ventas.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.scss']
})
export class VentasComponent implements OnInit {
  titulo = 'Ventas'
  parameter:any;
  sales:any = [];
  constructor(
  private router:Router,
  private location:Location,
  private modalController: ModalController,
  private route: ActivatedRoute,
  private mainService: VentasService
  ) { }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getAll();
  }
  
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }
  async presentModal(id?:any) {
    const modal = await this.modalController.create({
      component: ModalVentasComponent,
      componentProps: { value: id, cliente: this.parameter }
    });
    return await modal.present();
  }

  //CARGAR
  public getAll() {
    this.mainService.getAll()
    .then(response => {
      this.sales = [];
      this.sales = response;
    })
    .catch(error => {
      console.log(error)
    })
  }

}

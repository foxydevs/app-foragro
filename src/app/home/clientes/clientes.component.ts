import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { ModalController, AlertController } from '../../../../node_modules/@ionic/angular';
import { ModalClientesComponent } from './modal-clientes/modal-clientes.component';
import { ClientesService } from '../../_services/clientes.service';
import { MessagesService } from '../../_services/messages.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  titulo:string = 'Clientes'
  Table:any = [];
  idUser:any = localStorage.getItem('currentId')
  private itemSelected:string = 'mis';

  constructor(private router:Router,
  private location:Location,
  private modalController: ModalController,
  private messageService: MessagesService,
  private alertController: AlertController,
  private mainService: ClientesService) { }

  ngOnInit() {
    this.getAllMe();
  }

  segmentChanged(ev: any) {
    this.itemSelected = ev.detail.value;
    if(this.itemSelected == 'mis') {
      this.getAllMe();
    } else if(this.itemSelected == 'todos') {
      this.getAll();
    }
  }
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalClientesComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      if(data.data == 'Close') {
        this.getAllMe();
      }
    });
    return await modal.present();
  }

  //CARGAR
  public getAllMe() {
    this.messageService.present('Cargando...');
    this.mainService.getAllByUser(this.idUser)
    .then(response => {
      this.Table = [];
      this.Table = response;
      this.messageService.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR
  public getAll() {
    this.messageService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.Table = [];
      this.Table = response;
      this.messageService.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .then(response => {
      this.getAllMe();
    }).catch(error => {
      console.clear
    });
  }

  async confirmation(data:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar Cliente',
      message: '¿Desea eliminar al cliente '+ data.nombre + ' ' + data.apellido + ' ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data.id);
          }
        }
      ]
    });

    await alert.present();
  }

  //AGREGAR
  assignament(id:number) {
    let data = {
      clientes: [{id:id}],
      usuario: localStorage.getItem('currentId')
    }
    this.mainService.assignament(data)
    .then(response => {
      this.messageService.confirmation('Cliente Agregado', 'El cliente fue agregada exitosamente.');
    }).catch(error => {
      console.log(error)
    });
  }

}

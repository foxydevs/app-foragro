import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '../../../../../node_modules/@ionic/angular';
import { ClientesService } from '../../../_services/clientes.service';
import { MessagesService } from '../../../_services/messages.service';
import { Geolocation } from '../../../../../node_modules/@ionic-native/geolocation/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Subscription} from 'rxjs/Subscription';

declare var google;

@Component({
  selector: 'app-modal-clientes',
  templateUrl: './modal-clientes.component.html',
  styleUrls: ['./modal-clientes.component.scss']
})
export class ModalClientesComponent implements OnInit {
  titulo:string = 'Formulario Cliente';
  parameter:any;
  sectores:any[] = [];
  connected: Subscription;
  disconnected: Subscription;
  map: any;
  id:number = +localStorage.getItem('currentId');
  data = {
    nombre: '',
    apellido: '',
    nit: 0,
    municipio: '',
    departamento: '',
    direccion: '',
    telefono: '',
    celular: '',
    sector: '',
    longitud: 0,
    latitud: 0,
    pais: localStorage.getItem('currentPais'),
    usuario: this.id,
    id: 0,
    estado: 2
  }
  disabledBtn:boolean = false;

  constructor(
  private modalController: ModalController,
  private navParams: NavParams,
  private mainService: ClientesService,
  private messagesService: MessagesService,
  private geolocation: Geolocation,
  private network: Network,
  ) { }

  ngOnInit() {
    this.sectores = JSON.parse(localStorage.getItem('currentSectores'));
    this.parameter = this.navParams.get('value');
    this.checkNetworks();
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {
      this.getPosition();
    }
  }

  //SAVE CHANGES
  saveChanges() {
    this.data.usuario = +localStorage.getItem('currentId');
    if(this.data.nombre) {
      if(this.data.apellido) {
        if(this.parameter) {
          this.updated(this.data);
        } else {
          this.create(this.data);
        }
      } else {
        this.messagesService.presentToast('El apellido es requerido.');  
      }
    } else {
      this.messagesService.presentToast('El nombre es requerido.');
    }    
  }

  //GET SINGLE
  public getSingle(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getSingle(id)
    .then(response => {
      this.data = response;
      this.loadMapUpdate(response.latitud, response.longitud);
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .then(response => {
      this.messagesService.confirmation('Cliente Agregado', 'El cliente fue agregada exitosamente.');
      this.closeModal('Close');
    }).catch(error => {
      this.disabledBtn = false;
      this.messagesService.confirmation('Sin Conexión', 'El cliente fue agregado en la sección SIN CONEXIÓN.');
      this.createOffline(formValue);
      this.closeModal('Close');
      console.log(error)
    });
  }

  //AGREGAR
  createOffline(data:any) {
    let client:any[] = []
    if(localStorage.getItem('currentClientOffline')) {
      client = JSON.parse(localStorage.getItem('currentClientOffline'))
    }
    client.push(data)
    localStorage.removeItem('currentClientOffline')
    localStorage.setItem('currentClientOffline', JSON.stringify(client));
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.disabledBtn = true;
    this.mainService.update(formValue)
    .then(response => {
      this.messagesService.confirmation('Cliente Actualizado', 'El Cliente fue actualizado exitosamente.');
      this.closeModal('Close');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

  getPosition():any{
    this.geolocation.getCurrentPosition()
    .then(resp => {
      this.data.latitud = resp.coords.latitude;
      this.data.longitud = resp.coords.longitude;
      console.log(resp.coords.latitude + " dasdsa "+ resp.coords.longitude)
      this.loadMapUpdate(resp.coords.latitude, resp.coords.longitude);
     }).catch(error => {
    });
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.data.latitud = latitude.toString();
  this.data.longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitud = evt.latLng.lat();
      this.data.longitud = evt.latLng.lng();
    });
  }

  checkNetworks() {
    // watch network for a disconnect
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.messagesService.confirmation('network disconnected!', 'network was disconnected :-(');
    });

    // stop disconnect watch
    disconnectSubscription.unsubscribe();


    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      this.messagesService.confirmation('network connected!', '');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
          this.messagesService.confirmation('network connected!', 'we got a wifi connection, woohoo!');
        }
      }, 3000);
    });

    // stop connect watch
    connectSubscription.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController } from '@ionic/angular';
import { NotificationsService } from 'angular2-notifications';
import { EmpleadosService } from "../../_services/empleados.service";
import { MessagesService } from '../../_services/messages.service';
import { ClientesService } from '../../_services/clientes.service';
//GOOGLE
declare var google;
declare var $: any
@Component({
  selector: 'app-terminadas',
  templateUrl: './terminadas.component.html',
  styleUrls: ['./terminadas.component.scss']
})
export class TerminadasComponent implements OnInit {
  titulo:string = "Rutas Terminadas"
  Table: any;
  selectedData: any;
  selectedTag: any = "ventas";
  idUserApp:any = localStorage.getItem('currentId');
  //PROPIEDADES GOOGLE MAPS
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];
  clientes: any[];
  marker:any;
  labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  labelIndex = 0;

  constructor(
    public router: Router,
    public location:Location,
    public loadingController: LoadingController,
    public _service: NotificationsService,
    public mainService: ClientesService,
    public messageService: MessagesService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getAllClients();
  }
  
  active(select:any){
    this.selectedTag = select;
  }

  //CARGAR
  public getAllClients() {
    this.messageService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.clientes = [];
      response.forEach(x => {
        let data = {
          nombre: x.nombre + ' ' + x.apellido,
          direccion: x.direccion,
          id: x.id,
          estado: x.estado 
        }
        this.clientes.push(data)
      });
      this.messageService.dismiss();
    }).catch(error => {
      console.clear
    })
  }
  
  cargarAll(){
    // $('#Loading').css('display','block')
    // $('#Loading').addClass('in')
    this.Table = [
      {
        id:1,
        nombre: "Tienda 1",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:2,
        nombre: "Tienda 2",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:3,
        nombre: "Tienda 3",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:4,
        nombre: "Tienda 4",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      },
      {
        id:5,
        nombre: "Tienda 5",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      },
      {
        id:6,
        nombre: "Tienda 6",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      }
    ]
    // this.mainService.getAll()
    //                   .then(response => {
    //                     this.Table = response
    //                     $('#Loading').css('display','none')
    //                     console.clear
    //                   }).catch(error => {
    //                     console.clear
    //                     $('#Loading').css('display','none')
    //                     this.createError(error)
    //                   })
  }
  
  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        this.selectedData.name = response.title;

                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    formValue.title = formValue.name;
    this.mainService.update(formValue)
                      .then(response => {
                        console.clear
                        this.create('Producto Actualizado exitosamente')
                        $("#editModal .close").click();
                        $('#Loading').css('display','none')
                        this.cargarAll()
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

  }
  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea Eliminar el producto?")){
      this.mainService.delete(id)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Producto Eliminado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

    }else{
      $('#Loading').css('display','none')
    }

  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.title = formValue.name;
    this.mainService.create(formValue)
                      .then(response => {
                        console.clear
                        this.create('Producto Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form #name').val('');
                        $('#insert-form #description').val('');
                        $('#insert-form #price').val('');
                        $('#insert-form #category').val('');
                        $('#Loading').css('display','none')
                        this.cargarAll()
                        $("#insertModal .close").click();
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })


  }

public options = {
             position: ["bottom", "right"],
             timeOut: 2000,
             lastOnBottom: false,
             animate: "fromLeft",
             showProgressBar: false,
             pauseOnHover: true,
             clickToClose: true,
             maxLength: 200
         };

  create(success) {
              this._service.success('¡Éxito!',success)

  }
  createError(error) {
              this._service.error('¡Error!',error)

  }
}

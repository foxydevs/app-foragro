import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '../../../../../node_modules/@ionic/angular';
import { MessagesService } from '../../../_services/messages.service';
import { FormVisitaService } from '../../../_services/form-visita.service';
import { TipoAvanceService } from '../../../_services/tipo-avance.service';
import { TipoClienteService } from '../../../_services/tipo-cliente.service';
import { ObjetivosService } from '../../../_services/objetivos.service';
import { CultivosService } from '../../../_services/cultivos.service';
import { path } from '../../../config.module';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-ruta-detalle-planificacion',
  templateUrl: './modal-ruta-detalle-planificacion.component.html',
  styleUrls: ['./modal-ruta-detalle-planificacion.component.scss']
})
export class ModalRutaDetallePlanificacionComponent implements OnInit {
  titulo:string = 'Formulario Cliente';
  idUserApp:any = localStorage.getItem('currentId');
  parameter:any;
  data2:any;
  selectedTag:any = 'plan'
  cultivos:any = [];
  objetivos:any = [];
  basePath:string = path.path;
  data3 ={
    semanas: '',
    fechas: '',
    horas: '',
    nombre_clientes: '',
    lugars: '',
    departamentos: '',
    objetivo: '', 
    cultivos: '',
    foto1: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto2: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto3: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    observacion: '',
    tipo: '',
    tipo_clientes: '',
    tipo_avances: '',
  }
  data = {
    id: '',
    semana: '',
    fecha: '',
    hora: '',
    nombre_cliente: '',
    lugar: '',
    departamento: '',
    objetivos: '', 
    cultivo: '',
    observaciones: '',
    tipo: '',
    estado: 0,
    cliente: 1,
    tipo_cliente: '',
    tipo_avance: '',
    foto1: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto2: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto3: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    logrado: 1,
    razon: '',
    planificacion: '',
    usuario: localStorage.getItem('currentId'),
  }
  disabledBtn:boolean = false;
  tipoCliente:any = [];
  tipoAvance:any = [];
  
  constructor(
  private modalController: ModalController,
  private navParams: NavParams,
  private mainService: FormVisitaService,
  private messagesService: MessagesService,
  public secondService: TipoClienteService,
  public thirdService: TipoAvanceService,
  public fifthService: ObjetivosService,
  public sixthService: CultivosService,
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingle(this.parameter);
    //this.getAllSecond();
    //this.getAllThird();
    //this.getAllFifth();
    //this.getAllSixth();
    this.tipoCliente = JSON.parse(localStorage.getItem('currentTipoCliente'));
    this.tipoAvance = JSON.parse(localStorage.getItem('currentTipoAvance'));
    this.objetivos = JSON.parse(localStorage.getItem('currentTipoObjetivos'));
    this.cultivos = JSON.parse(localStorage.getItem('currentTipoCultivo'));
  }

  //GET DATE
  getDate() {
    let fechahoy = new Date();
    this.data.fecha = fechahoy.getFullYear() + '-' + (fechahoy.getMonth() + 1) + '-' + fechahoy.getDate();
    this.data.hora = fechahoy.getHours() + ':' + fechahoy.getMinutes() + ':' + fechahoy.getSeconds();
  }

  //SAVE CHANGES
  saveChanges() {
    this.data.foto1 = $('img[alt="Avatar"]').attr('src');
    this.data.foto2 = $('img[alt="Avatar2"]').attr('src');
    this.data.foto3 = $('img[alt="Avatar3"]').attr('src');
    this.create(this.data);
    let data = {
      logrado: this.data.logrado,
      id: this.data.id
    }
    this.updated(data)
  }

  //GET SINGLE
  public getSingle(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getSingle(id)
    .then(response => {
      this.data2 = response;
      this.data = response;
      this.data.foto1 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data.foto2 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data.foto3 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data3.semanas = response.semana;
      this.data3.fechas = response.fecha;
      this.data3.horas = response.hora;
      this.data3.nombre_clientes = response.nombre_cliente;
      this.data3.lugars = response.lugar;
      this.data3.departamentos = response.departamento;
      this.data3.objetivo = response.objetivo.nombre;
      this.data3.observacion = response.observaciones;
      this.data3.tipo_clientes = response.tipos_cliente.nombre;
      this.data3.tipo_avances = response.tipos_avance.nombre;
      this.data3.cultivos = response.cultivos.nombre;
      this.data.planificacion = this.parameter;
      this.getDate();
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .then(response => {
      this.messagesService.confirmation('Planificación Agregada', 'La planificación fue agregada exitosamente.');
      this.closeModal('Close');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
    }).catch(error => {
      console.clear
    });
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

  //CARGAR
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.tipoCliente = response;
    }).catch(error => {
      console.clear;
    })
  }
  
  public getAllThird(){
    this.thirdService.getAll()
    .then(response => {
      this.tipoAvance = response;
    }).catch(error => {
      console.clear;
    })
  }

  public getAllFifth(){
    this.fifthService.getAll()
    .then(response => {
      this.objetivos = response;
    }).catch(error => {
      console.clear;
    })
  }

  public getAllSixth(){
    this.sixthService.getAll()
    .then(response => {
      this.cultivos = response;
    }).catch(error => {
      console.clear;
    })
  }

  active(select:any){
    this.selectedTag = select;
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}/api/upload/${this.idUserApp}/usuarios`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta)
            $("#"+id).val('')
          }
        );
      } else {
        this.messagesService.presentToast('La imagen es demasiado grande.')
      }
    } else {
      this.messagesService.presentToast('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}/api/upload/${this.idUserApp}/usuarios`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            $('#imgAvatar2').attr("src", respuesta)
            $("#"+id).val('')
          }
        );
      } else {
        this.messagesService.presentToast('La imagen es demasiado grande.')
      }
    } else {
      this.messagesService.presentToast('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage3(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}/api/upload/${this.idUserApp}/usuarios`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar3').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            $('#imgAvatar3').attr("src", respuesta)
            $("#"+id).val('')
          }
        );
      } else {
        this.messagesService.presentToast('La imagen es demasiado grande.')
      }
    } else {
      this.messagesService.presentToast('El tipo de imagen no es válido.')
    }
  }
}

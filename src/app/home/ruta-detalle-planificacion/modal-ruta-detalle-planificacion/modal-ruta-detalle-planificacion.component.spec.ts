import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRutaDetallePlanificacionComponent } from './modal-ruta-detalle-planificacion.component';

describe('ModalRutaDetallePlanificacionComponent', () => {
  let component: ModalRutaDetallePlanificacionComponent;
  let fixture: ComponentFixture<ModalRutaDetallePlanificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRutaDetallePlanificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRutaDetallePlanificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

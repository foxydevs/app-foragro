import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaDetallePlanificacionComponent } from './ruta-detalle-planificacion.component';

describe('RutaDetallePlanificacionComponent', () => {
  let component: RutaDetallePlanificacionComponent;
  let fixture: ComponentFixture<RutaDetallePlanificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaDetallePlanificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaDetallePlanificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

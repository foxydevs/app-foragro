import { Component, OnInit } from '@angular/core';
import { FormVisitaService } from '../../_services/form-visita.service';
import { MessagesService } from '../../_services/messages.service';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { ModalController } from '../../../../node_modules/@ionic/angular';
import { ModalRutaDetallePlanificacionComponent } from './modal-ruta-detalle-planificacion/modal-ruta-detalle-planificacion.component';

@Component({
  selector: 'app-ruta-detalle-planificacion',
  templateUrl: './ruta-detalle-planificacion.component.html',
  styleUrls: ['./ruta-detalle-planificacion.component.scss']
})
export class RutaDetallePlanificacionComponent implements OnInit {
  titulo:string = "Planificación"
  visitas:any;
  parameter:any;
  selectedTag:any = 'pendientes';
  
  constructor(
    public mainService: FormVisitaService,
    public messagesService: MessagesService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public location: Location,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }
  active(select:any){
    this.selectedTag = select;
  }

  //CARGAR
  public getAll(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getAllClient(id)
    .then(response => {
      this.visitas = response;
      this.visitas.reverse();
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalRutaDetallePlanificacionComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController, AlertController } from '@ionic/angular';
import { NotificationsService } from 'angular2-notifications';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse} from '@ionic-native/background-geolocation/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { MessagesService } from '../../_services/messages.service';
import { CamionesService } from '../../_services/camiones.service';

@Component({
  selector: 'app-camiones',
  templateUrl: './camiones.component.html',
  styleUrls: ['./camiones.component.scss']
})
export class CamionesComponent implements OnInit {
  titulo = 'Camiones'
  Table:any;
  constructor(
    private router: Router,
    private location: Location,
    private mainService: CamionesService,
    private messageService: MessagesService,
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  //CARGAR
  public getAll() {
    this.messageService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.Table = response;
      this.messageService.dismiss();
    }).catch(error => {
      console.log(error)
      this.messageService.dismiss();
    })
  }
}

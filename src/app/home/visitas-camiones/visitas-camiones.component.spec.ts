import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitasCamionesComponent } from './visitas-camiones.component';

describe('VisitasCamionesComponent', () => {
  let component: VisitasCamionesComponent;
  let fixture: ComponentFixture<VisitasCamionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitasCamionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitasCamionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

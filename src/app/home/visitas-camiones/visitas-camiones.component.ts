import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController, ModalController, Platform, ActionSheetController } from '@ionic/angular';
import { NotificationsService } from 'angular2-notifications';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MessagesService } from '../../_services/messages.service';
import { ClientesService } from '../../_services/clientes.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

//GOOGLE
declare var google;
declare var $: any

@Component({
  selector: 'app-visitas-camiones',
  templateUrl: './visitas-camiones.component.html',
  styleUrls: ['./visitas-camiones.component.scss']
})
export class VisitasCamionesComponent implements OnInit {
  titulo:string = "Rutas"
  Table: any;
  selectedData: any;
  selectedTag: any = "visitar";
  idUserApp:any = localStorage.getItem('currentId');

  //PROPIEDADES GOOGLE MAPS
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];
  clientes: any[];
  visitas: any[];
  marker:any;
  labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  labelIndex = 0;
  latitude = 0;
  longitude = 0;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public location:Location,
    public loadingController: LoadingController,
    public _service: NotificationsService,
    public mainService: ClientesService,
    public modalController: ModalController,
    public geolocation: Geolocation,
    public messageService: MessagesService,
    public platform: Platform,
    public actionSheetController: ActionSheetController,
    public launchNavigator: LaunchNavigator
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }
  goToBack() {
    this.location.back();
  }

  //OPEN APP
  openLocation(latitude:any, longitude:any, data?:any) {
    let destination = latitude + ',' + longitude;
    if(this.platform.is('ios')){
      window.open('maps://?q=' + destination, '_system');
    } else {
      let label = encodeURI(data);
      window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
    }
  }

  async getLocation(latitude:any, longitude:any, data?:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'App de Ubicación',
      buttons: [
      {
        text: 'Google Maps',
        icon: 'map',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.GOOGLE_MAPS
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
            .then(
              success => console.log('Launched navigator'),
              error => console.log('Error launching navigator', error)
            );
        }
      }, {
        text: 'Waze',
        icon: 'pin',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.WAZE
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
            .then(
              success => console.log('Launched navigator'),
              error => console.log('Error launching navigator', error)
            );
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  getPosition():any{
    this.geolocation.getCurrentPosition()
    .then(resp => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      this.getAll(this.idUserApp, this.latitude, this.longitude);
      this.getAllClients(this.idUserApp, this.latitude, this.longitude);
     }).catch(error => {
      console.log('No location.');
    });
  }

  ngOnInit() {
    this.getPosition();
    setTimeout(() => {
      this.getMap();
      this.getMap2();
    }, 2000);
  }

  //CARGAR
  public getAll(id:any, lat:any, lng:any) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.messageService.present('Cargando...');
    this.mainService.getNear(id, lat, lng)
    .then(response => {
      this.waypoints = [];
      response.forEach(x => {
        if(x.latitud && x.estado == '1') {
          let data2 = {
            location: {lat: parseFloat(x.latitud), lng: parseFloat(x.longitud)},
            stopover: true,
          }
          this.waypoints.push(data2)
        }
      });
      this.messageService.dismiss();
    }).catch(error => {
      console.log('No location.');
    })
  }

  //CARGAR
  public getAllSecond() {
    this.messageService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.Table = [];
      this.Table = response;
      this.messageService.dismiss();
    }).catch(error => {
      console.log('No location.');
    })
  }

  //CARGAR
  public getAllClients(id:any, lat:any, lng:any) {
    this.messageService.present('Cargando...');
    this.mainService.getNear(id, lat, lng)
    .then(response => {
      this.clientes = [];
      response.forEach(x => {
        let data = {
          nombre: x.nombre,
          apellido: x.apellido,
          direccion: x.direccion,
          telefono: x.telefono,
          estado: x.estado,
          latitud: x.latitud, 
          longitud: x.longitud,
          distance: parseFloat(x.distance).toFixed(2) + ' K',
          id: x.id
        }
        this.clientes.push(data)
      })
      this.messageService.dismiss();
    }).catch(error => {
      console.log('No location.');
    })
  }

  segmentChanged(ev: any) {
    this.selectedTag = ev;
  }
  
  active(select:any){
    this.selectedTag = select;
  }
  async presentLoading(msg:string) {
    const loading = await this.loadingController.create({
      message: msg
    });
    return await loading.present();
  }

  //GOOGLE MAPS
  public initializeRoutes() {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    /*this.waypoints = [
      {
        location: { lat: 14.598063, lng: -90.524982 },
        stopover: true,
      },
      {
        location: { lat: 14.574794, lng: -90.549199 },
        stopover: true,
      },
      {
        location: { lat: 14.647253, lng: -90.451361 },
        stopover: true,
      },
      {
        location: { lat: 14.579674, lng: -90.494665 },
        stopover: true,
      },
      {
        location: { lat: 14.582737, lng: -90.555916 },
        stopover: true,
      }
    ];*/
  }

  
  getMap(){
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');
    let panelEle: HTMLElement = document.getElementById('panel');
  
    // create LatLng object
    this.myLatLng = {lat: this.latitude, lng: this.longitude};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 17
    });
  
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(panelEle);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.calculateRoute();
    });
  }

  getMap2(){
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map-diario');
    let panelEle: HTMLElement = document.getElementById('panel');
  
    // create LatLng object
    this.myLatLng = {lat: this.latitude, lng: this.longitude};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 17
    });
  
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(panelEle);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.calculateRoute();
    });
  }
  
  public calculateRoute(){
    if(this.waypoints.length) {
      this.bounds.extend(this.myLatLng);
  
      this.waypoints.forEach(waypoint => {
        var point = new google.maps.LatLng(waypoint.lat, waypoint.lng);
        this.bounds.extend(point);
      });
    
      this.map.fitBounds(this.bounds);
    
      this.directionsService.route({
        origin: new google.maps.LatLng(this.latitude, this.longitude),
        destination: new google.maps.LatLng(this.waypoints[this.waypoints.length -1].location.lat, this.waypoints[this.waypoints.length -1].location.lng),
        //destination: new google.maps.LatLng(this.clientes[this.clientes.length -1].location.lat, this.clientes[this.clientes.length -1].location.lng),
        waypoints: this.waypoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING,
        avoidTolls: true
      }, (response, status)=> {
        if(status === google.maps.DirectionsStatus.OK) {
          this.directionsDisplay.setDirections(response);
        }else{
          alert('Could not display directions due to: ' + status);
        }
      });  
    } else {
      this.messageService.presentToast('No existen datos cargados.');
    }  
  }
  cargarAll(){
    // $('#Loading').css('display','block')
    // $('#Loading').addClass('in')
    this.Table = [
      {
        id:1,
        nombre: "Tienda 1",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:2,
        nombre: "Tienda 2",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:3,
        nombre: "Tienda 3",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:1
      },
      {
        id:4,
        nombre: "Tienda 4",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      },
      {
        id:5,
        nombre: "Tienda 5",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      },
      {
        id:6,
        nombre: "Tienda 6",
        direccion: "3era avenida 6-45 ",
        distancia:5,
        estado:2
      }
    ]
    // this.mainService.getAll()
    //                   .then(response => {
    //                     this.Table = response
    //                     $('#Loading').css('display','none')
    //                     console.clear
    //                   }).catch(error => {
    //                     console.clear
    //                     $('#Loading').css('display','none')
    //                     this.createError(error)
    //                   })
  }
  
  cargarSingle(id:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        this.selectedData.name = response.title;

                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })
  }
  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    formValue.title = formValue.name;
    this.mainService.update(formValue)
                      .then(response => {
                        console.clear
                        this.create('Producto Actualizado exitosamente')
                        $("#editModal .close").click();
                        $('#Loading').css('display','none')
                        this.cargarAll()
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

  }
  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea Eliminar el producto?")){
      this.mainService.delete(id)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Producto Eliminado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })

    }else{
      $('#Loading').css('display','none')
    }

  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.title = formValue.name;
    this.mainService.create(formValue)
                      .then(response => {
                        console.clear
                        this.create('Producto Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form #name').val('');
                        $('#insert-form #description').val('');
                        $('#insert-form #price').val('');
                        $('#insert-form #category').val('');
                        $('#Loading').css('display','none')
                        this.cargarAll()
                        $("#insertModal .close").click();
                      }).catch(error => {
                        console.clear
                        $('#Loading').css('display','none')
                        this.createError(error)
                      })


  }

public options = {
             position: ["bottom", "right"],
             timeOut: 4000,
             lastOnBottom: false,
             animate: "fromLeft",
             showProgressBar: false,
             pauseOnHover: true,
             clickToClose: true,
             maxLength: 200
         };

  create(success) {
              this._service.success('¡Éxito!',success)

  }
  createError(error) {
              this._service.error('¡Error!',error)

  }

  doRefresh(event) {
    setTimeout(() => {
      this.getMap();
      event.target.complete();
    }, 2000);
  }

  doRefreshed(event) {
    setTimeout(() => {
      this.getMap2();
      event.target.complete();
    }, 2000);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormVisitaService } from '../../_services/form-visita.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { MessagesService } from '../../_services/messages.service';
import { ModalVisitasComponent } from '../visitas/modal-visitas/modal-visitas.component';
import { ModalController } from '../../../../node_modules/@ionic/angular';

@Component({
  selector: 'app-ruta-terminada-detalle',
  templateUrl: './ruta-terminada-detalle.component.html',
  styleUrls: ['./ruta-terminada-detalle.component.scss']
})
export class RutaTerminadaDetalleComponent implements OnInit {
  //PROPIEDADES
  selectedTag: any = "ventas";
  parameter:any;
  titulo:any = 'Detalles de Rutas';
  diario:any = [];
  visitas:any = [];

  constructor(
  public messagesService: MessagesService,
  public mainService: FormVisitaService,
  public router: ActivatedRoute,
  public location: Location,
  public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.parameter = this.router.snapshot.paramMap.get('id');
    this.getAll();
  }

  //ROUTES
  goToBack() {
    this.location.back();
  }
  active(select:any){
    this.selectedTag = select;
  }

  //CARGAR
  public getAll() {
    this.messagesService.present('Cargando...');
    this.mainService.getAll()
    .then(response => {
      this.visitas = [];
      this.visitas = response;
      this.visitas.reverse();
      this.messagesService.dismiss();
    }).catch(error => {
      console.clear;
      this.messagesService.dismiss();
    })
  }

  async presentModalVisita(id:number) {
    const modal = await this.modalController.create({
      component: ModalVisitasComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}

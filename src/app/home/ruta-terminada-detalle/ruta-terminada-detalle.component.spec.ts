import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaTerminadaDetalleComponent } from './ruta-terminada-detalle.component';

describe('RutaTerminadaDetalleComponent', () => {
  let component: RutaTerminadaDetalleComponent;
  let fixture: ComponentFixture<RutaTerminadaDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaTerminadaDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaTerminadaDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

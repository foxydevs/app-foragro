import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanificacionCamionesComponent } from './planificacion-camiones.component';

describe('PlanificacionCamionesComponent', () => {
  let component: PlanificacionCamionesComponent;
  let fixture: ComponentFixture<PlanificacionCamionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanificacionCamionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanificacionCamionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

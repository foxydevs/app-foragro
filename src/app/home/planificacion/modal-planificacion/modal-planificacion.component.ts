import { Component, OnInit } from '@angular/core';
import { FormVisitaService } from '../../../_services/form-visita.service';
import { MessagesService } from '../../../_services/messages.service';
import { NavParams, ModalController } from '../../../../../node_modules/@ionic/angular';
import { TipoClienteService } from '../../../_services/tipo-cliente.service';
import { TipoAvanceService } from '../../../_services/tipo-avance.service';
import { ObjetivosService } from '../../../_services/objetivos.service';
import { CultivosService } from '../../../_services/cultivos.service';

@Component({
  selector: 'app-modal-planificacion',
  templateUrl: './modal-planificacion.component.html',
  styleUrls: ['./modal-planificacion.component.scss']
})
export class ModalPlanificacionComponent implements OnInit {
  //PROPIEDADES
  disabledBtn:boolean = false;
  parameter:any;
  title:any;
  tipoCliente:any = [];
  tipoAvance:any = [];
  cultivos:any = [];
  objetivos:any = [];
  data = {
    id: '',
    semana: '',
    fecha: '',
    hora: '',
    nombre_cliente: '',
    lugar: '',
    departamento: '',
    objetivos: '', 
    cultivo: '',
    observaciones: '',
    tipo: '',
    estado: 0,
    cliente: 1,
    tipo_cliente: '',
    tipo_avance: '',
    usuario: localStorage.getItem('currentId'),
  }

  constructor(public mainService: FormVisitaService,
  public secondService: TipoClienteService,
  public thirdService: TipoAvanceService,
  public messagesService: MessagesService,
  public modalController: ModalController,
  public fifthService: ObjetivosService,
  public sixthService: CultivosService,
  public navParams: NavParams,) {
    //this.parameter = this.navParams.get('value')
  }

  closeModal() {
    this.modalController.dismiss();
  }

  ngOnInit() {
    this.getAllSecond();
    this.getAllThird();
    this.getAllFifth();
    this.getAllSixth();
    if(this.parameter) {
      this.title = 'Edición Planificación'
    } else {
      this.title = 'Agregar Planificación'
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.parameter) {
      this.update(this.data)      
    } else {
      this.create(this.data)
      console.log(this.data)
    }
  }

  //CARGAR
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.tipoCliente = response;
    }).catch(error => {
      console.clear;
    })
  }
  
  public getAllThird(){
    this.thirdService.getAll()
    .then(response => {
      this.tipoAvance = response;
    }).catch(error => {
      console.clear;
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.messagesService.confirmation('Planificación Agregada', 'La planificación fue agregada exitosamente.');
      this.title = 'Edición Planificación'
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.messagesService.confirmation('Planificación Actualizada', 'La planificación fue actualizada exitosamente.');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  public getAllFifth(){
    this.fifthService.getAll()
    .then(response => {
      this.objetivos = response;
    }).catch(error => {
      console.clear;
    })
  }

  public getAllSixth(){
    this.sixthService.getAll()
    .then(response => {
      this.cultivos = response;
    }).catch(error => {
      console.clear;
    })
  }

}
